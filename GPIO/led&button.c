#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"
/* Toggle LED by reading a button*/
// According the the datasheet pin 13 to pin 16 are buttons and pin 17 to 20 are leds
// there are 3 read configurations for the 2nd parameter
/*
|NRF_GPIO_PIN_PULLUP| =it is commonly paired with switches.
its purpose is to ensure the voltage between Ground and Vcc 
is actively controlled when the switch is open.
Connect between I/O pin and +supply voltage, with an open switch connected between I/O and ground.
Keeps the input “High”
----------------------------------------------------------
|NRF_GPIO_PIN_PULLDOWN| = Connect between an I/O pin and ground, with an open switch connected between I/O and +Supply.
Keeps the input “Low”
-------------------------------------------------------------
|NRF_GPIO_PIN_NOPULL| = no pull
*/
//we will use led 2 which is connected on pin 18
#define led2 18
#define button1 13

int main(void)
{
    nrf_gpio_cfg_output(led2);
    nrf_gpio_cfg_input(button1,NRF_GPIO_PIN_PULLUP);
    while (1)
    {
      while(nrf_gpio_pin_read(button1) == 0) {
        nrf_gpio_pin_write(18, 0 ? 1 : 0);//led on when button pressed    
      }
      nrf_gpio_pin_write(18, 0 ? 0 : 1);//led off on release
    }
}
