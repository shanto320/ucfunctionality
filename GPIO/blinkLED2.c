#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"
//led 1 and 3 does not work for given kit
int main(void)
{
    // 17 to 20 led pins
    nrf_gpio_cfg_output(18); // configure pin 18 as output
    
    while (true)
    {                         
            nrf_gpio_pin_write(18, 1 ? 0 : 1); // = nrf_gpio_pin_write(18, 0 ? 1 : 0);
                                              // understand ?: operator in C to understand the code
                                              // ?: = If Condition is true ? then value X : otherwise value Y
            nrf_delay_ms(500);
            nrf_gpio_pin_write(18, 1 ? 1 : 0);// = nrf_gpio_pin_write(18, 0 ? 0 : 1);
            nrf_delay_ms(500);

    }
}