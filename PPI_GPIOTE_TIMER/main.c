#include "globe.h"

void gpio_clock_xHz(uint32_t pin_number)
{
    
    NRF_TIMER3->PRESCALER = 0; // 16MHz
    NRF_TIMER3->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;
    NRF_TIMER3->CC[0] = 40;

    NRF_GPIOTE->CONFIG[0] = GPIOTE_CONFIG_MODE_Task | (pin_number << GPIOTE_CONFIG_PSEL_Pos) |
                            (GPIOTE_CONFIG_POLARITY_Toggle << GPIOTE_CONFIG_POLARITY_Pos);

    /*Connect TIMER event to GPIOTE out task*/
    NRF_PPI->CH[0].EEP = (uint32_t) &NRF_TIMER3->EVENTS_COMPARE[0];
    NRF_PPI->CH[0].TEP = (uint32_t) &NRF_GPIOTE->TASKS_OUT[0];
    NRF_PPI->CHENSET   = 1;

    /*Starts clock signal*/
    NRF_TIMER3->TASKS_START = 1;
}


int main(void) {
    board_init();

    //NRF_CLOCK->TASKS_HFCLKSTART = 1;
    //while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);


    gpio_clock_xHz(header_gpio_3_pin()); // use your preferred pin number instead of 27


    for (;;)
    {
        __WFE();
    }
    
    return 0;
}